

 In the form where comments are added, hide the username+label 
 since the user already knows who he is. IT make the UI cleaner.

 One can also hide the comment subject, in settings.php:
 $conf['comment_hideusername_hide_subject'] = TRUE; // hide the subject too ?
 this can also be done via the standard Drupal content types GUI


